﻿using System;

namespace BoardWepApp1.Models
{
    public class Paging
    {
        public int startPage { get; set; }
        public int endPage { get; set; } 
        public bool prev { get; set; }
        public bool next { get; set; }
        public int total { get; set; }
        public Pagenation pagenation { get; set; }

        public Paging(Pagenation pagenation,int total)
        {
            this.pagenation = pagenation;
            this.total = total; 

            this.endPage=(int)Math.Ceiling(pagenation.pageNum/10.0)*10;
            this.startPage = this.endPage - 9;
            int realEnd = (int)(Math.Ceiling((total * 1.0 / pagenation.amount)));
            if(realEnd < this.endPage) this.endPage = realEnd;
            this.prev = this.startPage > 1;
            this.next = this.endPage < realEnd;
        }

    }
}
