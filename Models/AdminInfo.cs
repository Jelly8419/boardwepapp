﻿namespace BoardWepApp1.Models
{
    public class AdminInfo
    {
        public int admin_num { get; set; }
        public string admin_name { get; set; }
        public string admin_id { get; set; }
        public string admin_password { get; set; }
        public string admin_phone { get; set; }
        public int admin_level { get; set; }

    }
}
