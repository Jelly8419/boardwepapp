﻿namespace BoardWepApp1.Models
{
    public class UserInfo
    {

        public int user_num { get; set; }
        public string user_id { get; set; } 
        public string user_name { get; set; }
        public string user_password { get; set; }
        public string user_email { get; set; }
        public string user_address { get; set; }
        public string user_phone { get; set; }
       
        public string user_birthDay { get; set; }

        public string user_type { get; set; }

        public int user_board_count { get; set; }

        public int user_reply_count { get; set; }

        public string user_profile { get; set; }

    }
}
