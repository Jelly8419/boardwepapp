﻿namespace BoardWepApp1.Models
{
    public class Pagenation
    {
        public int board_num { get; set; }// 댓글 가져올때만 사용됨 (게시글 해당x)
        public int pageNum { get; set; }
        public int amount { get; set; }
        public string type { get; set; }
        public string keyword { get; set; }

        /// <summary>
        /// 페이지네이션 생성자 오버로딩
        /// </summary>
        /// <param name="pageNum"></param>
        /// <param name="amount"></param>
        public Pagenation(int pageNum,int amount)
        {
            this.pageNum = pageNum;
            this.amount = amount;
        }
        public Pagenation()
        {
            this.pageNum = 1;
            this.amount = 20;
        }
      

       public string[] GetTypeArr()
        {
            return type == null ? new string[] { } : type.Split(" ");
        } 

      
    }
}





