﻿using Microsoft.AspNetCore.Http;
using System;

namespace BoardWepApp1.Models
{
    public class Report
    {
        public string report_num_list { get; set; }
        public int report_num { get; set; }
        public int report_board_num { get; set; }
        public string report_sender { get; set; }
        public string report_reason { get; set; }
        public string report_details { get; set; }
        public DateTime report_regdate { get; set; }
        public string report_result { get; set; }

        public string report_result_details { get; set; }

       
    }
}
