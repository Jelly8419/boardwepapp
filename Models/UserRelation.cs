﻿namespace BoardWepApp1.Models
{
    public class UserRelation
    {
        public int relation_num { get; set; }
        public string relation_type { get; set; }
        public string relation_sender_id { get; set; }
        public string relation_receiver_id { get; set; }
        public bool relation_accepted { get; set; }
       

  
    }
}
