﻿using System;

namespace BoardWepApp1.Models
{
    public class Reply
    {
        public int reply_num { get; set; }
        public int reply_board_num { get; set; }
        public string reply_replyer { get; set; }
        public string reply_content { get; set; }
        public DateTime reply_regdate  { get; set; }
        public int reply_depth { get; set; }
        public int reply_group { get; set; }

        public int reply_count { get; set; }

        
    }
}
