﻿using System;

namespace BoardWepApp1.Models
{
    public class Board
    {
        public int board_num { get; set; }
        public string board_title { get; set; }
        public string board_writer { get; set; }
        public string board_content { get; set; }
        public DateTime board_regdate { get; set; }
        public int board_views { get; set; }

        public string board_type { get; set; }
        public int board_reply_count { get; set; }//join으로 바꿔보기

    }
}
