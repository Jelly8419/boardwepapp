﻿namespace BoardWepApp1.Models
{
    public class Emotion
    {
        public int emotion_num { get; set; }
        public int emotion_board_num { get; set; }
        public string emotion_sender { get; set; }
        public bool emotion_recommended { get; set; }
        public bool emotion_not_recommended { get; set; }
        public bool emotion_upset { get; set; }
        public bool emotion_pleased { get; set; }
        public bool emotion_anger   { get; set; }


        //true인것만 가져오기
        public string GetTrue()
        {
            string result = "";
            if (emotion_recommended) result = "emotion_recommended";
            else if (emotion_not_recommended) result = "emotion_not_recommended";
            else if (emotion_upset) result = "emotion_upset";
            else if (emotion_pleased) result = "emotion_pleased";
            else if (emotion_anger) result = "emotion_anger";
            else result = "";

            return result;   
        }

        public bool DeleteEmotion()
        {
            return this.GetTrue() =="" ? true : false;
        }
    }
}
