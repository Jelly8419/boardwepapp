﻿using BoardWepApp1.Models;

//Session SetString/GetString
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace BoardWepApp1.Controllers
{
    public class AccountController : Controller
    {
        /// <summary>
        /// 회원가입 
        /// </summary>
        /// <returns></returns>
        public IActionResult SignUp()
        {
            return View();
        }
        
        /// <summary>
        /// 회원정보 수정 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult ModifyInfo()
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            
            var user_session = HttpContext.Session.GetString("user_session");
            UserInfo user = context.GetUserInfo(user_session);
            if (user_session != null && user.user_id == user_session)
            {
                return View(user);
            }
            else
            {
                TempData["msg"] = "세션값이 존재하지않습니다. 로그인 페이지로 이동합니다.";//한번읽고 사라짐
                return RedirectToAction("Login");
            }
        }

        /// <summary>
        /// 로그인
        /// </summary>
        /// <returns></returns>
        public IActionResult Login()
        {       
            return View();
        }
        /// <summary>
        /// 아이디 체크 Ajax처리
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public int IdCheck(UserInfo userInfo)
        {

            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            if (userInfo.user_id != null)
            {
                int count = context.IdCheck(userInfo.user_id);
                return count;
            }

            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 사용자 등록 페이지
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public IActionResult UserRegist(UserInfo userInfo)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            if (userInfo.user_id != null)
            {
                context.UserRegist(userInfo);
                TempData["msg"] = "회원가입 완료!";
                return View("Login");
            }
            else
            {
                TempData["msg"] = "회원등록 페이지로 이동합니다.";
                return RedirectToAction("SignUp");
            }

        }
        [HttpPost]
        public IActionResult UserModify(UserInfo userInfo)
        {
            var user_session = HttpContext.Session.GetString("user_session");
            if(user_session != null && user_session == userInfo.user_id)
            {
                DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
                context.UserModify(userInfo);
                HttpContext.Session.Clear();
                return View("Login");
            }
            else
            {
                TempData["msg"] = "지원하지 않는 접근입니다";
                return RedirectToAction("Index","Home");
            }
            
        }

        /// <summary>
        /// 로그인 시도 Ajax 처리
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>

        [HttpPost]
        public string LoginTry(UserInfo userInfo)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            string result=context.LoginTry(userInfo);
    
            if (result.Equals("Success"))
            {
                UserInfo user= context.GetUserInfo(userInfo.user_id);
                HttpContext.Session.SetString("user_session", userInfo.user_id);
                HttpContext.Session.SetString("user_type",user.user_type);
            }

            return result;

        }
        [HttpPost]
        public string FindId(UserInfo userInfo)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            return context.FindId(userInfo);
        }

        [HttpPost]
        public int FindPwd(UserInfo userInfo)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            return context.FindPwd(userInfo);
        }
        [HttpPost]
        public int ChangePwd(UserInfo userInfo)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            return context.ChangePwd(userInfo);
        }


        /// <summary>
        /// 로그아웃 
        /// </summary>
        /// <returns></returns>
        public RedirectResult Logout()
        {
            HttpContext.Session.Clear();
            return Redirect("/Board");
        }

        /// <summary>
        /// 유저 게시글수, 댓글수, 정보 가져오기
        /// </summary>
        /// <param name="user_num"></param>
        /// <returns></returns>
        [HttpGet]
        public UserInfo GetUserInfoWithPost(int user_num)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;

            try
            {
                return context.GetUserInfoWithPost(user_num);
            }catch(Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 유저 검색 결과 반환
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns></returns>
        [HttpGet]
        public List<UserInfo> GetSearchUserInfoList(string user_id)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof (DBService)) as DBService;

            try
            {
                return context.GetSearchUserInfoList(user_id);

            }catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 보낸 친구 요청 불러오기
        /// </summary>
        /// <param name="relation"></param>
        /// <returns></returns>
        [HttpGet]
        public List<UserInfo> GetUserRelationList(UserRelation relation)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            try
            {
                if (relation.relation_sender_id != HttpContext.Session.GetString("user_session"))
                {
                    TempData["msg"] = "로그인 세션이 만료되었습니다. 다시 로그인 해주세요";
                    return null;
                }
                else
                {
                    return context.GetUserRelationList(relation);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 받은 친구 요청 목록 불러오기
        /// </summary>
        /// <param name="userRelation"></param>
        /// <returns></returns>
        [HttpGet]
        public List<UserInfo> ReceiveRequest(UserRelation userRelation)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            try
            {
                if (userRelation.relation_sender_id != HttpContext.Session.GetString("user_session"))
                {
                    TempData["msg"] = "로그인 세션이 만료되었습니다. 다시 로그인 해주세요";
                    return null;
                }
                else
                {
                    return context.ReceiveRequest(userRelation);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public List<UserInfo> SendRequest(UserRelation userRelation)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            try
            {
                if (userRelation.relation_sender_id != HttpContext.Session.GetString("user_session"))
                {
                    TempData["msg"] = "로그인 세션이 만료되었습니다. 다시 로그인 해주세요";
                    return null;
                }
                
                else
                {
                    return context.SendRequest(userRelation);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AcceptRequest(UserRelation userRelation)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            try
            {
                if (HttpContext.Session.GetString("user_session") == null)
                {
                    TempData["msg"] = "세션 값이 존재하지 않습니다";
                    return false;
                }
                else
                {
                    return context.AcceptRequest(userRelation);
                }
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteRequest(UserRelation userRelation)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            try
            {
                if (HttpContext.Session.GetString("user_session") == null)
                {
                    TempData["msg"] = "세션 값이 존재하지 않습니다";
                    return false;
                }
                else
                {
                    return context.DeleteRequest(userRelation);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RelationRegist(UserRelation userRelation)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            try
            {
                if(HttpContext.Session.GetString("user_session") == null)
                {
                    TempData["msg"] = "세션 값이 존재하지 않습니다";
                    return false; 
                }
                else if (context.CheckRelationExist(userRelation))//이미 테이블에 존재할 경우
                {
                    return false;
                }
                else
                {
                    return context.RelationRegist(userRelation);
                }
            }catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
