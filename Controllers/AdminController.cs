﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using System;
using BoardWepApp1.Models;
using System.Collections.Generic;

namespace BoardWepApp1.Controllers
{
    public class AdminController : Controller
    {
        public IActionResult AdminLogin()
        {
            HttpContext.Session.Clear();
           return View();
        }

        [HttpPost]
        public int AdminLoginTry(AdminInfo adminInfo)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            int result = context.AdminLonginTry(adminInfo);

            if (result== 1)
            {
                AdminInfo admin = context.GetAdminInfo(adminInfo.admin_id);
                HttpContext.Session.SetString("admin_session", adminInfo.admin_id);
                HttpContext.Session.SetString("admin_level", adminInfo.admin_level+"");
            }

            return result;
        }
        public IActionResult ManageReport(Pagenation pagenation)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            try
            {
                if(HttpContext.Session.GetString("admin_session")!= null)
                {
                    pagenation = pagenation.amount == 0 ? new Pagenation() : pagenation ;
                    int count = context.GetReportCount();
                    List<Report>reportList= context.GetReportListWithPage(pagenation);

                    ViewBag.PageMaker = new Paging(pagenation, count);
                    
                    return View("ManageReport",reportList);
                }
                else
                {
                    TempData["msg"] = "허용되지 않은 접근입니다";
                    return RedirectToAction("Index","Home");
                }
            }catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]

        public IActionResult AdminReportUpdate(Report report,Pagenation pagenation)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            try
            {

                if (HttpContext.Session.GetString("admin_session")!=null && context.AdminReportUpdate(report))
                {
                    TempData["msg"] = "처리 완료";
                    return ManageReport(pagenation);
                } 
                else
                {
                    TempData["msg"] = "권한이 없습니다";
                    return RedirectToAction("Index", "Home");
                }
                    
            }catch (Exception ex)
            {
                throw ex;
            }
        }
        public IActionResult ManageUser(Pagenation pagenation)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            try
            {
                if(HttpContext.Session.GetString("admin_session") != null)
                {
                    int count = context.GetUserCount();
                    if (pagenation == null) pagenation = new Pagenation();
                    List<UserInfo> userList = context.GetUserListWithPage();
                    ViewBag.PageMaker = new Paging(pagenation, count);
                    return View(userList);
                }
                else
                {
                    TempData["msg"] = "잘못된 접근입니다";
                    return RedirectToAction("Index", "Home");
                }
            }catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
