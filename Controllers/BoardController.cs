﻿using BoardWepApp1.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text.Encodings.Web;

namespace BoardWepApp1.Controllers
{
    public class BoardController : Controller
    {
        
        //GET : /Board/
        public IActionResult Index(Pagenation pagenation)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            int count = context.GetTotalCount(pagenation);
            string user_session = HttpContext.Session.GetString("user_session");
            if(pagenation == null) pagenation = new Pagenation();


            ViewBag.NoticeList = context.GetNoticeList();
            ViewBag.PageMaker = new Paging(pagenation, count);
            return View(context.GetBoardListWithPage(pagenation,user_session));
        }
        public IActionResult Regist()
        {
            if (HttpContext.Session.GetString("user_session") != null || HttpContext.Session.GetString("admin_session") != null)
            {
                return View();
            }
            else
            {
                TempData["msg"] = "로그인 정보가 확인되지 않아 로그인 페이지로 이동합니다.";
                return RedirectToAction("Login", "Account");
            }

        }

        [HttpPost]
        public JsonResult UploadImageFile(IFormFile file)
        {
          
            var uploadPath = "BoardImages";
            var count = 0;
            var filePath = System.IO.Path.Combine(uploadPath, file.Name);
            var k = 1;
            try
            {
               
                while (System.IO.File.Exists(filePath))
                {
                    var name = file.FileName.Substring(0, file.FileName.IndexOf('.'));
                    var ext = file.FileName.Substring(file.FileName.LastIndexOf('.')+1);
                    filePath = uploadPath + $"/{name}({ k++}).{ext}";
                }
                using(var uploadFile = System.IO.File.Create(filePath))
                {
                    file.CopyTo(uploadFile);
                }
                count++;
            }
            catch(Exception ex)
            {
              return Json(ex.Message);
            }
            return Json("/"+filePath);
        }
        /// <summary>
        /// 게시글 등록시 호출
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult BoardRegist(Board board)
        {
            
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            if (HttpContext.Session.GetString("user_session") != null || HttpContext.Session.GetString("admin_session") != null) 
            {
                context.BoardRegist(board);
                return RedirectToAction("Index");
            }
            else
            {
                TempData["msg"] = "로그인정보가 확인되지 않아 로그인 페이지로 이동합니다";
                return RedirectToAction("Login","Account");
            }
           
        }
        /// <summary>
        /// 게시글 조회
        /// </summary>
        /// <param name="board_num"></param>
        /// <returns></returns>
        ///검색결과에서 게시글 조회 후 목록으로 돌아갔을때, 검색결과 그대로 나오게 하기위해 pagenation파라미터 추가로 전달
        public IActionResult Details(int board_num,Pagenation pagenation)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            
            Board board = context.GetBoard(board_num);
            board.board_reply_count = context.GetReplyCount(board_num);
            Emotion emotion = new Emotion();
            Report report = new Report();

            var remote_ip_address = Request.HttpContext.Connection.RemoteIpAddress; //Ip 못가져옴 수정하기( 3/23일)

            
            var views_updated = HttpContext.Session.GetString((remote_ip_address.ToString() + board_num));
            if(views_updated == null)
            {
                if(context.UpdateBoardViews(board_num) == 1)
                    HttpContext.Session.SetString((remote_ip_address.ToString() + board_num), "1");
            }
            
            emotion.emotion_sender = HttpContext.Session.GetString("user_session");
            emotion.emotion_board_num = board_num;
            report.report_sender = HttpContext.Session.GetString("user_session");
            report.report_board_num = board_num;

            emotion = context.GetEmotion(emotion);//감정표현 가져오기
            report = context.GetReport(report);//신고내용 가져오기

            int count = context.GetTotalCount(pagenation);

            if (pagenation == null) pagenation = new Pagenation();
            ViewBag.PageMaker = new Paging(pagenation, count);
            ViewBag.Emotion = emotion;
            ViewBag.Report = report;

            return View(board);
        }
       
        /// <summary>
        /// 게시글 수정 View 
        /// </summary>
        /// <param name="board_num"></param>
        /// <returns></returns>
        public IActionResult Modify(int board_num,Pagenation pagenation)
        {
            string user_session = HttpContext.Session.GetString("user_session");
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            Board board = context.GetBoard(board_num);
            if (user_session == board.board_writer || HttpContext.Session.GetString("admin_session") != null)
            {
                int count = context.GetTotalCount(pagenation);
                if (pagenation == null) pagenation = new Pagenation();
                ViewBag.PageMaker = new Paging(pagenation, count);
                return View(board);
            }
            else
            {
                TempData["msg"] = "잘못된 접근입니다.";
                return RedirectToAction("Index", "Home");
            }

        }
        /// <summary>
        /// 게시글 업데이트 클릭시 호출 메소드 
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public IActionResult BoardUpdate(Board board)
        {

            string user_session = HttpContext.Session.GetString("user_session");
            if (user_session == board.board_writer || HttpContext.Session.GetString("admin_session") != null)
            {
                DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
                context.BoardUpdate(board);
                return RedirectToAction("Details", new { board.board_num });
            }
            else
            {
                TempData["msg"] = "로그인정보가 확인되지 않아 로그인 페이지로 이동합니다";
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// 감정표현 생성,수정,삭제
        /// </summary>
        /// <param name="emotion"></param>
        /// <returns></returns>
        [HttpPost]
        public Emotion EmotionUpdate(Emotion emotion)
        {
            
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            context.EmotionUpdate(emotion);
            return context.GetEmotion(emotion);
        }
        /// <summary>
        /// 게시글 신고 생성,수정
        /// </summary>
        /// <param name="report"></param>
        /// <returns></returns>
        [HttpPost]
        public Report ReportUpdate(Report report)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            context.ReportUpdate(report);

            return context.GetReport(report);
        }
        /// <summary>
        /// 게시글 신고 삭제
        /// </summary>
        /// <param name="report"></param>
        /// <returns></returns>
        [HttpPost]
        public Report ReportDelete(Report report)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            context.ReportDelete(report);

            return context.GetReport(report);
        }

        /// <summary>
        /// 댓글 가져오기
        /// </summary>
        /// <param name="board_num"></param>
        /// <returns></returns>
        /// 

        public List<Object> GetReplyListWithPage(Pagenation pagenation)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            
            try
            {
                string user_session = HttpContext.Session.GetString("user_session");
                
                int count = context.GetReplyCount(pagenation.board_num);
                
                Paging paging = new Paging(pagenation,count);
                List<Object> returnList = new List<Object>();
                List<Reply> replyList = context.GetReplyListWithPage(pagenation, user_session);
                returnList.Add(replyList);
                returnList.Add(paging);
                return returnList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        /// <summary>
        /// 댓글등록
        /// </summary>
        /// <param name="reply"></param>
        /// <returns></returns>
        [HttpPost]     
        public bool ReplyRegist(Reply reply)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            try
            {
                return context.ReplyRegist(reply);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 댓글 업데이트
        /// </summary>
        /// <param name="reply"></param>
        /// <returns></returns>
        [HttpPost]
        public bool ReplyUpdate(Reply reply)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            try
            {
               return context.ReplyUpdate(reply);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       /// <summary>
       /// 댓글 삭제 처리
       /// </summary>
       /// <param name="reply"></param>
       /// <returns></returns>
        [HttpPost]
        public bool ReplyDelete(Reply reply)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            try
            {
                return context.ReplyDelete(reply);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

}
