﻿using BoardWepApp1.Models;
using Microsoft.AspNetCore.Http;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace BoardWepApp1
{
    public class DBService
    {
       public string ConnectionString { get; set; }

        public DBService(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            
            MySqlConnection conn = new MySqlConnection(ConnectionString);
            conn.Open(); 
            return conn; 
        }


        public int GetUserCount()
        {
            string sql = "select count(*) from userInfo";

            using(MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);

                return  Convert.ToInt32(cmd.ExecuteScalar());
            }
        }

        /// <summary>
        /// 회원 리스트 가져오기
        /// </summary>
        /// <returns></returns>
        public List<UserInfo> GetUserListWithPage()
            
        {   
            List<UserInfo> list = new List<UserInfo>();
            string sql = "SELECT * FROM userInfo ";
            
            using (MySqlConnection connection = GetConnection())
            {

                MySqlCommand cmd = new MySqlCommand(sql, connection);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new UserInfo()
                        {
                            user_num = Convert.ToInt32(reader["user_num"]),
                            user_id = reader["user_id"].ToString(),
                            user_name = reader["user_name"].ToString(),
                            user_email = reader["user_email"].ToString(),  
                            user_phone = reader["user_phone"].ToString(),
                            user_address = reader["user_address"].ToString(),
                            user_type = reader["user_type"].ToString()
                        });//end Add
                    }//end While
                }
                return list;
            }
        }

        /// <summary>
        /// 회원정보 수정에 사용되는 유저정보 가져오기
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserInfo GetUserInfo(string id)
        {
            string sql = $"select * from UserInfo where user_id = '{id}'";
            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                var reader= cmd.ExecuteReader();
                if (reader.Read())
                {
                    return new UserInfo()
                    {
                        user_id = reader["user_id"].ToString(),
                        user_name = reader["user_name"].ToString(),
                        user_password = reader["user_password"].ToString(),
                        user_address = reader["user_address"].ToString(),
                        user_email = reader["user_email"].ToString(),
                        user_phone = reader["user_phone"].ToString(),
                        user_type = reader["user_type"].ToString(),
                        user_profile = reader["user_profile"].ToString()
                    };
                }
                else
                {
                    return null;
                }
            }
          
        }
        /// <summary>
        /// 회원정보, 게시글 수, 댓글 수 가져오기
        /// </summary>
        /// <param name="board_num"></param>
        /// <returns></returns>
        public UserInfo GetUserInfoWithPost(int user_num)
        {
            string sql = $"select user_num,user_id,user_profile, user_name, count(distinct board_num) as user_board_count, count(distinct reply_num) as user_reply_count " +
                $"from userInfo left join board on userinfo.user_id = board.board_writer " +
                $"left join board_reply on userinfo.user_id =board_reply.reply_replyer where user_num = {user_num} ";
            using(MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);

                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return new UserInfo()
                    {
                        user_num = Convert.ToInt32(reader["user_num"]),
                        user_id = reader["user_id"].ToString(),
                        user_name = reader["user_name"].ToString(),
                        user_board_count = Convert.ToInt32(reader["user_board_count"]),
                        user_reply_count = Convert.ToInt32(reader["user_reply_count"]),
                        user_profile = reader["user_profile"].ToString()
                    };
                }
                else return null;
            }
        }
        /// <summary>
        /// 검색 결과 리스트에 띄우기 위한 메서드
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns></returns>
        public List<UserInfo> GetSearchUserInfoList(string user_id)
        {
            string sql = $"select user_num,user_id,user_name,user_profile from userInfo where user_id like '%{user_id}%' ";

            using(MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);

                List<UserInfo> userList = new List<UserInfo>();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    userList.Add(new UserInfo()
                    {
                        user_num = Convert.ToInt32(reader["user_num"]),
                        user_id = reader["user_id"].ToString(),
                        user_name = reader["user_name"].ToString(),
                        user_profile = reader["user_profile"].ToString()
                    });
                }
                return userList;
            }
        }
        /// <summary>
        /// 유저간의 관계를 
        /// </summary>
        /// <param name="userRelation"></param>
        /// <returns></returns>
        public List<UserInfo> GetUserRelationList(UserRelation userRelation)//친구 목록, 차단 목록 불러올때 사용
        {
            string sql = $"select user_num,user_id,user_name,user_profile from userInfo " +
                $"left join user_relation on user_id = relation_sender_id  or user_id = relation_receiver_id " +
                    $"where (relation_sender_id ='{userRelation.relation_sender_id}' or relation_receiver_id = '{userRelation.relation_sender_id}') and " +
                    $"(relation_type = '{userRelation.relation_type}' and relation_accepted = {userRelation.relation_accepted} and user_id != '{userRelation.relation_sender_id}' )";

            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                List<UserInfo> userList = new List<UserInfo>();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    userList.Add(
                           new UserInfo()
                           {
                               user_num = Convert.ToInt32(reader["user_num"]),
                               user_name = reader["user_name"].ToString(),
                               user_id = reader["user_id"].ToString(),
                               user_profile = reader["user_profile"].ToString()
                           }
                        );
                }
                return userList;

            }

        }

        public List<UserInfo> ReceiveRequest (UserRelation userRelation)
        {
            string sql = $"select user_num,user_id,user_name,user_profile from userInfo " +
              $"left join user_relation on user_id = relation_sender_id  or user_id = relation_receiver_id " +
                  $"where ( relation_receiver_id = '{userRelation.relation_sender_id}') and " +
                  $"(relation_type = '{userRelation.relation_type}' and relation_accepted = {userRelation.relation_accepted} and user_id != '{userRelation.relation_sender_id}' )";
            
            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                List<UserInfo> userList = new List<UserInfo>();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    userList.Add(
                           new UserInfo()
                           {
                               user_num = Convert.ToInt32(reader["user_num"]),
                               user_name = reader["user_name"].ToString(),
                               user_id = reader["user_id"].ToString(),
                               user_profile = reader["user_profile"].ToString()
                           }
                        );
                }
                return userList;

            }
        }

        public List<UserInfo> SendRequest(UserRelation userRelation)
        {
            string sql = $"select user_num,user_id,user_name,user_profile from userInfo " +
              $"left join user_relation on user_id = relation_sender_id  or user_id = relation_receiver_id " +
                  $"where ( relation_sender_id = '{userRelation.relation_sender_id}') and " +
                  $"(relation_type = '{userRelation.relation_type}' and relation_accepted = {userRelation.relation_accepted} and user_id != '{userRelation.relation_sender_id}' )";

            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                List<UserInfo> userList = new List<UserInfo>();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    userList.Add(
                           new UserInfo()
                           {
                               user_num = Convert.ToInt32(reader["user_num"]),
                               user_name = reader["user_name"].ToString(),
                               user_id = reader["user_id"].ToString(),
                               user_profile = reader["user_profile"].ToString()
                           }
                        );
                }
                return userList;

            }
        }
        public bool AcceptRequest(UserRelation userRelation)
        {
            string sql = $"update  user_relation set relation_accepted = true " +
                             $"where relation_sender_id = '{userRelation.relation_sender_id}' and relation_receiver_id = '{userRelation.relation_receiver_id}' ";
            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                if (cmd.ExecuteNonQuery() >= 1)
                    return true;
                else
                    return false;
            }
        }

        public bool DeleteRequest(UserRelation userRelation)
        {
            string sql = $"delete from user_relation" +
                         $" where (relation_sender_id = '{userRelation.relation_sender_id}' or relation_sender_id = '{userRelation.relation_receiver_id}' ) " +
                         $" and (relation_receiver_id = '{userRelation.relation_sender_id}' or relation_receiver_id = '{userRelation.relation_receiver_id}' ) ";
            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                if (cmd.ExecuteNonQuery() >= 1)
                    return true;
                else
                    return false;
            }
        }

        public bool RelationRegist(UserRelation userRelation)
        {
            string sql = $"insert into user_relation(relation_type, relation_sender_id, relation_receiver_id,relation_accepted  ) " +
                $"values ('{userRelation.relation_type}', '{userRelation.relation_sender_id}', '{userRelation.relation_receiver_id}', {userRelation.relation_accepted})";
            using(MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                if (cmd.ExecuteNonQuery() >= 1) 
                   return true;
                else 
                  return false;
            }
        }

        public bool CheckRelationExist(UserRelation userRelation)
        {
            string sql = $"select * from user_relation " +
                         $"where (relation_sender_id = '{userRelation.relation_sender_id}' or relation_sender_id = '{userRelation.relation_receiver_id}' ) " +
                         $" and (relation_receiver_id = '{userRelation.relation_sender_id}' or relation_receiver_id = '{userRelation.relation_receiver_id}' ) ";
            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                var reader = cmd.ExecuteReader();
                if(reader.Read())
                    return true;
                else
                    return false;
            }
        }












        /// <summary>
        /// 아이디 중복체크
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int IdCheck(string id)
        {
            string sql = $"select Count(*) from (select user_id from userInfo union select admin_id from adminInfo) id where user_id = '{id}'";//관리자테이블 추가
            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                return count;
            }
        }

       
        /// <summary>
        /// 회원 가입 정보등록
        /// </summary>
        /// <param name="userInfo"></param>
        public void UserRegist(UserInfo userInfo)
        {
            string sql = $"insert into UserInfo(user_id,user_name,user_birthDay,user_password,user_address,user_email,user_phone ) " +
                            $"  values('{userInfo.user_id}','{userInfo.user_name}','{userInfo.user_birthDay}', " +
                                $"'{userInfo.user_password}','{userInfo.user_address}'," +
                                $"'{userInfo.user_email}', '{userInfo.user_phone}' ) ";
            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                cmd.ExecuteNonQuery();
            }
        }
        public void UserModify(UserInfo userInfo)
        {
            string sql = $"update userInfo set user_address = '{userInfo.user_address}', user_profile = '{userInfo.user_profile}', " +
                             $"user_email = '{userInfo.user_email}', user_phone = '{userInfo.user_phone}', user_password = '{userInfo.user_password}' " +
                             $"where user_id = '{userInfo.user_id}' ";
            using(MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// 로그인 시도 아이디,비밀번호 체크
        /// </summary>
        /// <param name="userInfo">유저의 정보 model</param>
        /// <returns></returns>
        public string LoginTry(UserInfo userInfo)
        {
            string sql = $"select * from userInfo where user_id = '{userInfo.user_id}'";
            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                var reader = cmd.ExecuteReader();
                string result = "";
                if (reader.Read())
                {
                    if (!reader["user_id"].ToString().Equals("null") && reader["user_password"].ToString() != userInfo.user_password)
                    {
                        result = "비밀번호가 틀렸습니다";
                    }
                    else
                    {
                        
                        result = "Success";
                    }
                }
                else
                {
                    result = "존재하지 않는 아이디입니다";
                }
                return result;
            }
        }
        public string FindId(UserInfo userInfo)
        {
            string sql = $"select user_id from userInfo where user_name ='{userInfo.user_name}' and user_phone = '{userInfo.user_phone}' ";
            using (MySqlConnection connection = GetConnection())
            {
                
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                var reader = cmd.ExecuteReader();
                string result = "";

                if (reader.Read())
                {
                    result = "회원님의 아이디는"+reader["user_id"].ToString()+"입니다";
                    
                }
                else
                {
                    result = "존재하지않는 회원정보입니다";
                }
                
                return result;
            }
        }

        public int FindPwd(UserInfo userInfo)
        {
            string sql = $"select count(*) from userInfo where user_id = '{userInfo.user_id}' and user_phone = '{userInfo.user_phone}' ";
            using (MySqlConnection connection = GetConnection())
            {
                
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                int count= Convert.ToInt32(cmd.ExecuteScalar());
                
                return count;
            }
        }

        public int ChangePwd(UserInfo userInfo)
        {
            string sql = $"update userInfo set user_password ='{userInfo.user_password}' where user_id = '{userInfo.user_id}'";
            using(MySqlConnection connection = GetConnection())
            {
                
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                int count = cmd.ExecuteNonQuery();
                
                return count;
            }
        }

        ///////////////////////////////////////////////////   AccountEnd ///////////////////////////////////////////////////////////////////////



        

        /// <summary>
        /// 게시글 등록 
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        public int BoardRegist(Board board)
        {
            board.board_type = board.board_type == null ? "common" : board.board_type;
            string sql = $"insert into board(board_writer,board_title,board_content,board_type) " +
                             $"values ('{board.board_writer}', '{board.board_title}','{board.board_content}','{board.board_type}')";

            using ( MySqlConnection connection = GetConnection())
            {
                
                MySqlCommand cmd = new MySqlCommand(sql,connection);
                int count = cmd.ExecuteNonQuery();
                
                return count;
            }
        }
        /// <summary>
        /// 게시글 목록 가져오기 
        /// </summary>
        /// <param name="pagenation"></param>
        /// <returns></returns>
        public List<Board> GetBoardListWithPage(Pagenation pagenation, string user_session)
        {
            List<string> insertSQLList = new List<string>();
            string []typeArr = pagenation.GetTypeArr();
            foreach(var type in typeArr)
            {
                switch (type)
                {
                    case "T": insertSQLList.Add($"board_title like '%{pagenation.keyword}%' ");
                        break;
                    case "W": insertSQLList.Add($"board_writer like '%{pagenation.keyword}%' ");
                        break;
                    case "C": insertSQLList.Add($"board_content like '%{pagenation.keyword}%' ");
                        break;
                    default:
                        break;
                        
                }
            }
            string insertSQL= String.Join(" OR ", insertSQLList.ToArray());
            StringBuilder sql = new StringBuilder();

            sql.Append($"select * from board where board_type='common' and board_writer not in(" +
                $"select relation_receiver_id from user_relation where relation_sender_id='{user_session}' and relation_type='block' ) ");
            if (insertSQL != "")
                sql.Append($"and ({insertSQL}) ");       
            
            sql.Append($"order by board_num desc limit {((pagenation.pageNum-1) * pagenation.amount)}, {pagenation.amount}");
            List<Board> list = new List<Board>();
            using (MySqlConnection connection = GetConnection())
            {
                
                MySqlCommand cmd = new MySqlCommand(sql.ToString(), connection);
               
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Console.WriteLine(reader["board_regdate"]);
                        list.Add(new Board()
                        {
                            board_num = Convert.ToInt32(reader["board_num"]),
                            board_writer = reader["board_writer"].ToString(),
                            board_title = reader["board_title"].ToString(),
                            board_content = reader["board_content"].ToString(),
                            board_regdate =  Convert.ToDateTime(reader["board_regdate"]),
                            board_views = Convert.ToInt32(reader["board_views"])
                        });//end Add
                    }//end While
                }
                
                return list;
            }
        }
        /// <summary>
        /// 게시글 조회하기
        /// </summary>
        /// <param name="board_num"></param>
        /// <returns></returns>
        public Board GetBoard(int board_num)
        {
            string sql= $"select * from Board where board_num ={board_num}";
            using (MySqlConnection connection = GetConnection())
            {
                
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return new Board()
                    {
                        board_writer = reader["board_writer"].ToString(),
                        board_num = Convert.ToInt32(reader["board_num"]),
                        board_content = reader["board_content"].ToString(),
                        board_title = reader["board_title"].ToString(),
                        board_regdate = Convert.ToDateTime(reader["board_regdate"]),
                        board_views = Convert.ToInt32(reader["board_views"])
                    };
                }
                else return null;
            }

        }
        /// <summary>
        /// 세션값 존재하지않으면 조회수 증가
        /// </summary>
        /// <param name="board_num"></param>
        /// <returns></returns>
        public int UpdateBoardViews(int board_num)
        {
            string sql = $"update board set board_views = board_views+1 where board_num = {board_num}";

            using(MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                return cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// 게시글 수정
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        public int BoardUpdate(Board board)
        {
            string sql = $"update board set " +
                               $"board_title ='{board.board_title}', board_content ='{board.board_content}'" +
                                    $"where board_num = {board.board_num}";
            using (MySqlConnection connection = GetConnection())
            {
                
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                int count = cmd.ExecuteNonQuery();
                
                return count;
            }
        }
        /// <summary>
        /// 게시글 수 가져오기
        /// </summary>
        /// <returns></returns>
        public int GetTotalCount(Pagenation pagenation)
        {
            string sql = "select count(*) from board";
            List<string> insertSQLList = new List<string>();
            string[] typeArr = pagenation.GetTypeArr();
            foreach (var type in typeArr)
            {
                switch (type)
                {
                    case "T":
                        insertSQLList.Add($"board_title like '%{pagenation.keyword}%' ");
                        break;
                    case "W":
                        insertSQLList.Add($"board_writer like '%{pagenation.keyword}%' ");
                        break;
                    case "C":
                        insertSQLList.Add($"board_content like '%{pagenation.keyword}%' ");
                        break;
                    default:
                        break;

                }
            }
            string insertSQL = String.Join(" OR ", insertSQLList.ToArray());
            if (insertSQL != "") sql += $" where {insertSQL}";
            using (MySqlConnection connection = GetConnection())
            {
                
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                var reader = cmd.ExecuteScalar();
            

                
                return Convert.ToInt32(reader);
            }
        }

        /// <summary>
        /// 클라이언트의 요청에따른 감정표현데이터 테이블에 추가,업데이트,삭제
        /// </summary>
        /// <param name="emotion"></param>
        public void EmotionUpdate(Emotion emotion)
        {
            bool deleteData = emotion.DeleteEmotion();
            string updateData = emotion.GetTrue();  
            using (MySqlConnection connection = GetConnection())
            {
                string sql = "";
                if (emotion.emotion_num == 0)//감정표현이 테이블에 없을때 
                {
                      sql = $"insert into board_emotion(emotion_board_num,{updateData},emotion_sender ) values({emotion.emotion_board_num},true,'{emotion.emotion_sender}') ";
                }
                else
                {
                    if (deleteData)//
                    {
                        sql = $"delete from board_emotion where emotion_num = {emotion.emotion_num}";
                    }
                    else
                    {
                        sql = $"update board_emotion set " +
                           $"emotion_upset = {emotion.emotion_upset}, emotion_pleased = {emotion.emotion_pleased}," +
                           $"emotion_anger = {emotion.emotion_anger}, emotion_recommended = {emotion.emotion_recommended}, emotion_not_recommended = {emotion.emotion_not_recommended}" +
                           $" where emotion_num = {emotion.emotion_num} ";
                    }
                }
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                cmd.ExecuteNonQuery();
                
            }

        }

        /// <summary>
        /// 해당게시글에 감정표현 여부확인하기 위한 서비스
        /// </summary>
        /// <param name="emotion"></param>
        /// <returns></returns>
        public Emotion GetEmotion(Emotion emotion)
        {
            string sql = $"select * from board_emotion where emotion_board_num = {emotion.emotion_board_num} and emotion_sender = '{emotion.emotion_sender}' ";
            using (MySqlConnection connection = GetConnection())
            {
                
                Emotion emotion1 = new Emotion();
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    emotion1.emotion_num = Convert.ToInt32(reader["emotion_num"]);
                    emotion1.emotion_board_num = Convert.ToInt32(reader["emotion_board_num"]);
                    emotion1.emotion_sender = reader["emotion_sender"].ToString();
                    emotion1.emotion_pleased = Convert.ToInt32(reader["emotion_pleased"]) == 1 ? true : false;
                    emotion1.emotion_anger = Convert.ToInt32(reader["emotion_anger"]) == 1 ? true : false;
                    emotion1.emotion_upset = Convert.ToInt32(reader["emotion_upset"]) == 1 ? true : false;
                    emotion1.emotion_recommended = Convert.ToInt32(reader["emotion_recommended"]) == 1 ? true : false;
                    emotion1.emotion_not_recommended = Convert.ToInt32(reader["emotion_not_recommended"]) == 1 ? true : false;
                }
                
                return emotion1;
            }
        }

        public void ReportUpdate(Report report)
        {
            using (MySqlConnection connection = GetConnection())
            {
                string sql = "";

                if (report.report_num == 0)//신고내역이 테이블에 없을때 
                {
                    sql = $"insert into board_report(report_board_num,report_sender,report_reason,report_details ) " +
                          $"values({report.report_board_num},'{report.report_sender}','{report.report_reason}','{report.report_details}') ";
                }
                else
                {
                    sql = $"update board_report set " +
                        $" report_reason = '{report.report_reason}', report_details ='{report.report_details}' " +
                        $" where report_num = {report.report_num} ";
                   
                }
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                cmd.ExecuteNonQuery();
                
            }
        }

        public void ReportDelete(Report report)
        {
            string sql = $"delete from board_report where report_num = {report.report_num} ";
            using (MySqlConnection connection = GetConnection())
            {
                
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                cmd.ExecuteNonQuery();
                
            }
            
        }

        public Report GetReport(Report report)
        {
            string sql = $"select * from board_report where report_board_num={report.report_board_num} and report_sender='{report.report_sender}'";
            using(MySqlConnection connection = GetConnection())
            {
                
                Report report1 = new Report();
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                { 
                    report1.report_num = Convert.ToInt32(reader["report_num"]);
                    report1.report_board_num = Convert.ToInt32(reader["report_board_num"]);
                    report1.report_sender = reader["report_sender"].ToString();
                    report1.report_details = reader["report_details"].ToString();
                    report1.report_reason = reader["report_reason"].ToString();
                }
                
                return report1;
            }
        }
        /// <summary>
        /// 댓글 등록
        /// </summary>
        /// <param name="reply"></param>
        /// <returns></returns>
        public bool ReplyRegist(Reply reply)
        {
            string sql = $"insert into board_reply (reply_board_num, reply_replyer, reply_content, reply_group, reply_depth) values" +
                $" ({reply.reply_board_num}, '{reply.reply_replyer}', '{reply.reply_content}',{reply.reply_group},{reply.reply_depth}) ";

            using(MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                if(cmd.ExecuteNonQuery() != 0)
                {
                    sql = "update board_reply set reply_group = reply_num where reply_group=0";//나중에 바꾸기 
                    cmd= new MySqlCommand(sql, connection);
                    cmd.ExecuteNonQuery();
                    return true;
                  
                }
                else 
                    return false;
            }
        }


        public List<Reply> GetReplyListWithPage(Pagenation pagenation, string user_session)
        {
            string sql = $"select  *from board_reply  where reply_board_num = {pagenation.board_num} and " +
                        $"reply_replyer not in(select relation_receiver_id from user_relation where relation_sender_id ='{user_session}' and relation_type='block' ) " +
                            $"order by reply_group limit {((pagenation.pageNum - 1) * pagenation.amount)}, {pagenation.amount} ";
        
            using(MySqlConnection connection = GetConnection())
            {
                List<Reply> replyList = new List<Reply>();
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    replyList.Add(new Reply()
                    {
                        reply_num = Convert.ToInt32(reader["reply_num"]),
                        reply_board_num = Convert.ToInt32(reader["reply_board_num"]),
                        reply_replyer = reader["reply_replyer"].ToString(),
                        reply_content = reader["reply_content"].ToString(),
                        reply_regdate = Convert.ToDateTime( reader["reply_regdate"]),
                        reply_depth = Convert.ToInt32(reader["reply_depth"]),
                        reply_group = Convert.ToInt32(reader["reply_group"])
                    }); 
                }
                return replyList;
            }
        }
        public bool ReplyUpdate(Reply reply)
        {
            string sql = $"update board_reply set reply_content = '{reply.reply_content}' where reply_num={reply.reply_num} and reply_replyer='{reply.reply_replyer}' ";
            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                if (cmd.ExecuteNonQuery() != 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }
        /// <summary>
        /// 댓글 삭제 
        /// </summary>
        /// <param name="reply"></param>
        /// <returns></returns>
        public bool ReplyDelete(Reply reply)
        {

            string sql = $"delete from board_reply where  ( reply_num = {reply.reply_num} or reply_group={reply.reply_num} )  and reply_replyer='{reply.reply_replyer}'";
            
            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                if (cmd.ExecuteNonQuery() != 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }

        public int GetReplyCount(int board_num)
        {
            string sql = $"select count(*) from board_reply where reply_board_num = {board_num} ";
            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                return  Convert.ToInt32(cmd.ExecuteScalar());
            }
        }


        public int AdminLonginTry(AdminInfo adminInfo)
        {
            string sql = $"select count(*) from adminInfo where admin_id='{adminInfo.admin_id}' and admin_password='{adminInfo.admin_password}'";

            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
        }

        public AdminInfo GetAdminInfo(string admin_id)
        {
            string sql = $"select * from AdminInfo where admin_id = '{admin_id}'";
            using (MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return new AdminInfo()
                    {
                        admin_num = Convert.ToInt32(reader["admin_num"]),
                        admin_id = reader["admin_id"].ToString(),
                        admin_level = Convert.ToInt32(reader["admin_level"]),
                        admin_name = reader["admin_name"].ToString(),
                        admin_phone = reader["admin_phone"].ToString()
                    };
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// 게시글 중 공지사항 만 가져오기
        /// </summary>
        /// <returns></returns>
        public List<Board> GetNoticeList()
        {
            string sql = $"select*from board where board_type = 'notice' order by board_num desc limit 5";
            
            using(MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                List<Board> noticeList = new List<Board>();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    noticeList.Add(new Board()
                    {
                        board_num = Convert.ToInt32(reader["board_num"]),
                        board_writer = reader["board_writer"].ToString(),
                        board_title = reader["board_title"].ToString(),
                        board_content = reader["board_content"].ToString(),
                        board_regdate = Convert.ToDateTime(reader["board_regdate"]),
                        board_views = Convert.ToInt32(reader["board_views"])

                    });
                }
                return noticeList;
            }
        }

        public List<Report> GetReportListWithPage(Pagenation pagenation)
        {
            string sql = $"select * from board_report order by report_num desc limit {((pagenation.pageNum - 1) * pagenation.amount)}, {pagenation.amount}";
            using(MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);

                var reader = cmd.ExecuteReader();
                List<Report> reportList = new List<Report>();
                while (reader.Read())
                {
                    reportList.Add(new Report()
                    {
                        report_num = Convert.ToInt32(reader["report_num"]),
                        report_board_num = Convert.ToInt32(reader["report_board_num"]),
                        report_sender = reader["report_sender"].ToString(),
                        report_reason = reader["report_reason"].ToString(),
                        report_details = reader["report_details"].ToString(),
                        report_regdate = Convert.ToDateTime(reader["report_regdate"]),
                        report_result = reader["report_result"].ToString()
                    });
                }
                return reportList;
            }
        }
        /// <summary>
        /// 신고 GetCount
        /// </summary>
        /// <returns></returns>
        public int GetReportCount()
        {
            string sql = "select count(*) from board_report ";
            
            using(MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
        }

        public bool AdminReportUpdate(Report report)
        {
            string sql = report.report_result != "삭제" ?// 관리자가 신고처리를 삭제로했으면 delete, 그렇지않으면 update
                $"update board_report set report_result='{report.report_result}' , report_result_details = '{report.report_result_details}' " :
                $"delete from board_report";
                sql += $" where report_num in({report.report_num_list})"; 

            using(MySqlConnection connection = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);

                if (cmd.ExecuteNonQuery() >= 1)
                    return true;
               
                else
                    return false;
            }
        }
    }
    
}
